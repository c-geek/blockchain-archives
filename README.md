# Blockchain archive

This git repository is a blockchain archive for Duniter. It stores old blocks for third-party usages, for example it is used for Duniter CI tests.
